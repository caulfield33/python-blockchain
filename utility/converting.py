from block import Block
from transaction import Transaction


def to_block_chain_object(json_chain):
    updated_blockchain = []
    for block in json_chain:
        updated_block = Block(block['index'], block['previous_hash'], to_transactions_object(block['transactions']), block['proof'], block['timestamp'])
        updated_blockchain.append(updated_block)

    return updated_blockchain


def to_transactions_object(json_transactions):
    updated_transactions = [Transaction(json_transaction['sender'], json_transaction['recipient'], json_transaction['signature'], json_transaction['amount']) for json_transaction in json_transactions]
    return updated_transactions


def to_dict_chain(chain):
    dict_transactions = [Block(block.index, block.previous_hash, to_dict_transactions(block.transactions), block.proof, block.timestamp) for block in chain]
    return to_dict_transactions(dict_transactions)


def to_dict_transactions(transactions):
    dict_chain = [block.__dict__ for block in transactions]
    return dict_chain

