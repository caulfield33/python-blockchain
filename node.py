from flask import Flask, jsonify, request, send_from_directory
from flask_cors import CORS

from wallet import Wallet
from blockchain import BlockChain
import utility.converting as converting

app = Flask(__name__)
CORS(app)


@app.route('/', methods=['GET'])
def get_node_ui():
    return send_from_directory('ui', 'node.html')


@app.route('/network', methods=['GET'])
def get_network_ui():
    return send_from_directory('ui', 'network.html')


@app.route('/wallet', methods=['POST'])
def create_keys():
    wallet.create_keys()

    if wallet.save_keys():
        global block_chain
        block_chain = BlockChain(wallet.public_key, port)
        response = {
            'public_key': wallet.public_key,
            'private_key': wallet.private_key,
            'funds': block_chain.get_balance()
        }
        return jsonify(response), 201

    else:
        response = {
            'message': 'Saving the keys failed.'
        }
        return jsonify(response), 500


@app.route('/wallet', methods=['GET'])
def load_keys():
    if wallet.load_keys():
        global block_chain
        block_chain = BlockChain(wallet.public_key, port)
        response = {
            'public_key': wallet.public_key,
            'private_key': wallet.private_key,
            'funds': block_chain.get_balance()
        }
        return jsonify(response), 201

    else:
        response = {
            'message': 'Loading the keys failed.'
        }
        return jsonify(response), 500


@app.route('/balance', methods=['GET'])
def get_balance():
    balance = block_chain.get_balance()
    if balance != None:
        response = {
            'message': 'Fetched balance successfully.',
            'funds': balance
        }
        return jsonify(response), 200
    else:
        response = {
            'messsage': 'Loading balance failed.',
            'wallet_set_up': wallet.public_key != None
        }
        return jsonify(response), 500


@app.route('/broadcast-transaction', methods=['POST'])
def broadcast_transaction():
    values = request.get_json()
    if not values:
        response = {
            'message': 'No data found.'
        }
        return jsonify(response), 400

    required = ['sender', 'recipient', 'amount', 'signature']
    if not all(key in values for key in required):
        response = {
            'message': 'Some data is missing.'
        }
        return jsonify(response), 400

    success = block_chain.add_transaction(values['recipient'], values['sender'], values['signature'], values['amount'], is_receiving=True)
    if success:
        response = {
            'message': 'Successfully added transaction.',
            'transaction': {
                'sender': values['sender'],
                'recipient': values['recipient'],
                'amount': values['amount'],
                'signature': values['signature']
            }
        }
        return jsonify(response), 201

    else:
        response = {
            'message': 'Creating a transaction failed.'
        }
        return jsonify(response), 500


@app.route('/broadcast-block', methods=['POST'])
def broadcast_block():
    values = request.get_json()
    if not values:
        response = {
            'message': 'No data found.'
        }
        return jsonify(response), 400

    if 'block' not in values:
        response = {
            'message': 'Some data is missing.'
        }
        return jsonify(response), 400

    block = values['block']
    if block['index'] == block_chain.chain[-1].index + 1:
        if block_chain.add_block(block):
            response = {
                'message': 'Block added'
            }
            return jsonify(response), 201

        else:
            response = {
                'message': 'Block seems invalid.'
            }
            return jsonify(response), 409

    elif block['index'] > block_chain.chain[-1].index:
        response = {
            'message': 'block_chain seems to differ from local block_chain.'
        }
        block_chain.resolve_conflict = True
        return jsonify(response), 200

    else:
        response = {
            'message': 'block_chain seems to be shorter, block not added'
        }
        return jsonify(response), 409


@app.route('/transaction', methods=['POST'])
def add_transaction():
    if wallet.public_key == None:
        response = {
            'message': 'No wallet set up.'
        }
        return jsonify(response), 400

    values = request.get_json()
    if not values:
        response = {
            'message': 'No data found.'
        }
        return jsonify(response), 400

    required_fields = ['recipient', 'amount']
    if not all(field in values for field in required_fields):
        response = {
            'message': 'Required data is missing.'
        }
        return jsonify(response), 400

    recipient = values['recipient']
    amount = values['amount']
    signature = wallet.sign_transaction(wallet.public_key, recipient, amount)
    success = block_chain.add_transaction(recipient, wallet.public_key, signature, amount)
    if success:
        response = {
            'message': 'Successfully added transaction.',
            'transaction': {
                'sender': wallet.public_key,
                'recipient': recipient,
                'amount': amount,
                'signature': signature
            },
            'funds': block_chain.get_balance()
        }
        return jsonify(response), 201

    else:
        response = {
            'message': 'Creating a transaction failed.'
        }
        return jsonify(response), 500


@app.route('/mine', methods=['POST'])
def mine():
    if block_chain.resolve_conflict:
        response = {
            'message': 'Resolve conflict before mine block.'
        }
        return jsonify(response), 409

    block = block_chain.mine_block()
    if block != None:
        dict_block = block.__dict__.copy()
        dict_block['transactions'] = converting.to_dict_transactions(dict_block['transactions'])
        response = {
            'message': 'Block added successfully.',
            'block': dict_block,
            'funds': block_chain.get_balance()
        }
        return jsonify(response), 201

    else:
        response = {
            'message': 'Adding a block failed.',
            'wallet_set_up': wallet.public_key != None
        }
        return jsonify(response), 500


@app.route("/resolve-conflicts", methods=["POST"])
def resolve_conflict():
    replaced = block_chain.resolve()

    if replaced:
        response = {
            'message': 'Chain was replaced.',
        }
        return jsonify(response), 200
    else:
        response = {
            'message': 'Local chain kept.',
        }
        return jsonify(response), 200


@app.route('/transactions', methods=['GET'])
def get_open_transaction():
    transactions = block_chain.get_open_transactions()
    return jsonify(converting.to_dict_transactions(transactions)), 200


@app.route('/chain', methods=['GET'])
def get_chain():
    return jsonify(converting.to_dict_chain(block_chain.chain)), 200


@app.route('/node', methods=['POST'])
def add_node():
    values = request.get_json()
    if not values:
        response = {
            'message': 'No data attached.'
        }
        return jsonify(response), 400

    if 'node' not in values:
        response = {
            'message': 'No node data found.'
        }
        return jsonify(response), 400

    node = values['node']
    block_chain.add_peer_node(node)
    response = {
        'message': 'Node added successfully.',
        'nodes': block_chain.get_peer_nodes()
    }
    return jsonify(response), 201


@app.route('/node/<node_url>', methods=['DELETE'])
def remove_node(node_url):
    if node_url == '' or node_url == None:
        response = {
            'message': 'No node found.'
        }
        return jsonify(response), 400

    block_chain.remove_peer_node(node_url)
    response = {
        'message': 'Node removed',
        'all_nodes': block_chain.get_peer_nodes()
    }
    return jsonify(response), 200


@app.route('/nodes', methods=['GET'])
def get_nodes():
    nodes = block_chain.get_peer_nodes()
    response = {
        'nodes': nodes
    }
    return jsonify(response), 200


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=5000)
    args = parser.parse_args()
    port = args.port
    wallet = Wallet(port)
    block_chain = BlockChain(wallet.public_key, port)
    app.run(host='0.0.0.0', port=port)
